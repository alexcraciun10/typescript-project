import Store, { Question } from "../store/configuration/store";

const questions: Question[] = [{
    id: 1,
    text: "Ce tip de obiect doriti?",
    choices: [{
        text: "Fructe",
        imgUrl: "http://storage0.dms.mpinteractiv.ro/media/2/2/24986/11639082/5/fructe1.jpg?height=353&width=470"
    }, {
        text: "Legume",
        imgUrl: "https://cdn1.medicalnewstoday.com/content/images/articles/320/320302/a-selection-of-fruits-and-vegetables.jpg"
    }]
}, {
    id: 2,
    text: "Ce buget implica?",
    choices: [{
        text: "Ieftin ca braga",
        imgUrl: "https://a1.ro/uploads/modules/news/0/2017/9/4/686183/150453464922d8536c.jpg"
    }, {
        text: "Degeaba furi la cantar",
        imgUrl: "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/766/everything-you-need-to-know-about-cherries-main-1498164926.jpg?resize=480:*"
    }]
}, {
    id: 3,
    text: "Ce flexibilitate cautati?",
    choices: [{
        text: "Regina ciorbei",
        imgUrl: "https://savoriurbane.com/wp-content/uploads/2018/01/Ciorba-de-potroace-reteta-traditionala-savori-urbane.jpg"
    }, {
        text: "Nici in salata nu merge",
        imgUrl: "https://retete.unica.ro/wp-content/uploads/2016/11/saaa-1024x707.jpg"
    }]
}, {
    id: 4,
    text: "Ce dinamism doriti?",
    choices: [{
        text: "De savurat si pe longboard",
        imgUrl: "https://previews.123rf.com/images/olegevseev/olegevseev1607/olegevseev160700031/60566319-man-riding-on-longboard-in-the-streets-urban-setting-of-lifestyle-concept.jpg"
    }, {
        text: "Posibil doar de pe scaun",
        imgUrl: "https://images.ctfassets.net/6m9bd13t776q/2harHCrcReSmGQGQWcQgYG/968563beb73bc212bbbdedbc2e69be41/when-start-solid-food-baby-eating-2160x1200.jpg?q=75"
    }]
}, {
    id: 5,
    text: "Ce impact social cautati?",
    choices: [{
        text: "Festin cu gasca",
        imgUrl: "https://i.ytimg.com/vi/EYiZeszLosE/maxresdefault.jpg"
    }, {
        text: "Cringe Sharapova",
        imgUrl: "http://3.bp.blogspot.com/-pBaQCkbd2-A/TuTm-nJJf3I/AAAAAAAABg8/QvY0UjGY_RY/s1600/maria+sharapova+banana+phallic+deep+throat+blowjob.jpg"
    }]
}, {
    id: 6,
    text: "Caracterul traditional al produsului?",
    choices: [{
        text: "Prea ca la tara",
        imgUrl: "http://blog.hotelguru.ro/wp-content/uploads/2014/11/Mancaruri-traditionale-din-Bucovina.jpg"
    }, {
        text: "Molecule la pachet",
        imgUrl: "http://img.finedininglovers.com/?img=http%3A%2F%2Ffinedininglovers.cdn.crosscast-system.com%2FBlogPost%2FOriginal_4723_TP-molecular-kitchen.jpg&w=1200&h=660&lu=1365506025&ext=.jpg"
    }]
}];

const data: Store = {
    questions,
    activeQuestionId: 1
};

export default data;