import Store from "./store";

const initialStore: Store = {
    questions: [],
    activeQuestionId: 1
};

export default initialStore;