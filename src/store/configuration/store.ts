interface Store {
    questions: Question[];
    activeQuestionId: number;
}

export interface Question {
    id: number;
    text: string;
    choices: Choice[];
}

export interface Choice {
    text: string;
    imgUrl: string;
}

export default Store;