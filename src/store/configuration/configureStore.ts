import { combineReducers, compose, createStore } from 'redux';
import initialStore from './initialStore';
import storeReducer from '../reducers/store';
import {composeWithDevTools} from 'redux-devtools-extension';

/** Store */
const store = createStore(
    storeReducer,
    initialStore,
    composeWithDevTools()
);

export default store;