import Store, { Question } from "../configuration/store";
import { AnyAction } from "redux";
import initialStore from "../configuration/initialStore";
import { find, map, size } from "lodash";

// Interfaces
interface SetStoreAction {
    type: string;
    payload: Store;
}

// Actions
const SET_STORE = "questions/SET_STORE";
const SET_NEXT_ACTIVE_QUESTION = "questions/SET_NEXT_ACTIVE_QUESTION";

// Action Creators
export const setStore = (store: Store): SetStoreAction => ({
    type: SET_STORE,
    payload: store
});

export const setNextQuestionActive = (): AnyAction => ({
    type: SET_NEXT_ACTIVE_QUESTION
});

// Reducer
const store = (state: Store = initialStore, action: AnyAction): Store => {
    switch(action.type) {
        case SET_STORE:
            const setStoreAction = action as SetStoreAction;
            return setStoreAction.payload;

        case SET_NEXT_ACTIVE_QUESTION:
            const activeQuestionId = state.activeQuestionId + 1;

            return {
                ...state,
                activeQuestionId
            }

        default:
            return state;
    }
};

export default store;