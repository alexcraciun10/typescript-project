import { find, isEmpty, size } from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import data from '../../../mock/data';
import Store, { Question } from '../../../store/configuration/store';
import { setStore } from '../../../store/reducers/store';
import If from '../../functional/If';
import SmartQuestionCard from '../QuestionCard';
import { Container } from './style';
import Result from '../Result';

interface StoreProps {
    // Properties
    questions: Question[];
    activeQuestionId: number;

    // Action Creators
    setStore: (store: Store) => any;
}

type Props = StoreProps;

/**
 * On mount sets the data for the questions and renders the active question.
 * When the questions have finished, render the final result.
 */
const App = (props: Props) => {
    React.useEffect(() => { props.setStore(data) }, []);

    const activeQuestion = find(props.questions, (question) => question.id === props.activeQuestionId);

    // If we answered the last question, render the result
    const shouldRenderResult = props.activeQuestionId > size(props.questions);
    const shouldRenderQuestions = !isEmpty(activeQuestion) && !shouldRenderResult;

    return (
        <Container>
            <If condition={shouldRenderQuestions}>
                <SmartQuestionCard question={activeQuestion as Question} />
            </If>

            <If condition={shouldRenderResult}>
                <Result />
            </If>
        </Container>
        
    );
};

const mapStateToProps = (store: Store) => ({
    questions: store.questions,
    activeQuestionId: store.activeQuestionId
});

const mapDispatchToProps = {
    setStore
};

export default connect(mapStateToProps, mapDispatchToProps)(App);