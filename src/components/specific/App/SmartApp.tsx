import * as React from 'react';
import { Provider } from 'react-redux';
import App from './App';
import store from '../../../store/configuration/configureStore';

/**
 * Embeddes the entire application with the Redux store.
 */
const SmartApp = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

export default SmartApp;