import * as React from 'react';
import { Choice } from '../../../store/configuration/store';
import { Container, Image, Text } from './style';

interface Props {
    choice: Choice;
    onClick: () => any;
}

/**
 * Presentational implementation for a choice, contains the title and the
 * image as elements. When clicking on the image, the user will see a new question.
 * @param props contains the choice and the behavior on click from the parent component.
 */
const ChoiceOption = (props: Props) => (
    <Container>
        <Text>{props.choice.text}</Text>

        <Image
            src={props.choice.imgUrl}
            onClick={props.onClick}
        />
    </Container>
);

export default ChoiceOption;