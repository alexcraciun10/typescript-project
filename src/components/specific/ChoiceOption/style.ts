import styled from 'styled-components';

export const Image = styled.img`
    height: 200px;
    width: 300px;
    border-radius: 5%;
    background-size: auto 100%;
    cursor: pointer;
    display: block;
    margin-left: auto;
    margin-right: auto;

    &:hover {
        box-shadow: 10px 10px 5px grey;
    }
`;

export const Text = styled.h2`
    font-family: Roboto;
    text-align: center;
`;

export const Container = styled.div`
    padding: 20px;
    width: 400px;
`;