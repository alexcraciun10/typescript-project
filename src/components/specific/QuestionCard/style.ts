import styled from 'styled-components';

export const QuestionText = styled.h1`
    font-family: Roboto;
    text-align: center;
`;

export const Container = styled.div`
    position: absolute;
    display: block;
    margin-left: auto;
    margin-right: auto;

    opacity: 1;
	animation-name: fadeInOpacity;
	animation-iteration-count: 1;
	animation-timing-function: ease-in;
    animation-duration: 2s;

    @keyframes fadeInOpacity {
        0% { opacity: 0; }
        100% { opacity: 1; }
    }
`;