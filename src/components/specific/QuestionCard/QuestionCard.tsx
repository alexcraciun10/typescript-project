import * as React from 'react';
import { Choice, Question } from '../../../store/configuration/store';
import Map from '../../functional/Map';
import ChoiceOption from '../ChoiceOption';
import { QuestionText, Container } from './style';

interface Props {
    question: Question;
    onChoiceClick: () => any;
}

/**
 * The presentational implementation for a question.
 * @param props contains the question and the behavior
 * when clicking on a choice.
 */
const QuestionCard = (props: Props) => {
    const { id, choices, text } = props.question;

    const onMap = (choice: Choice) => (
        <ChoiceOption
            choice={choice}
            onClick={props.onChoiceClick}
        />
    );

    return (
        <Container>
            <QuestionText>{id}. {text}</QuestionText>

            <Map 
                collection={choices}
                mapFunction={onMap}
            />
        </Container>
    );
};

export default QuestionCard;