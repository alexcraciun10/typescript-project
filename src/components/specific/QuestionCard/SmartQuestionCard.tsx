import * as React from 'react';
import QuestionCard from './QuestionCard';
import { Question } from '../../../store/configuration/store';
import { setNextQuestionActive } from '../../../store/reducers/store';
import { connect } from 'react-redux';

interface ComponentProps {
    question: Question;
}

interface StoreProps {
    setNextQuestionActive: () => any;
}

type Props = ComponentProps & StoreProps;

/**
 * Smart component for rendering the questions and when
 * clicking on a choice, we set the next question active in store.
 * @param props contains the question from the parent component and
 * the action creator  from the Redux.
 */
const SmartQuestionCard = (props: Props) => (
    <QuestionCard
        question={props.question}
        onChoiceClick={props.setNextQuestionActive}
    />
);

const mapDispatchToProps = {
    setNextQuestionActive
};

export default connect(null, mapDispatchToProps)(SmartQuestionCard);