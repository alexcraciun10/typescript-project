import * as React from 'react';
import { Container, Text, TypeScriptLogo } from './style';

/**
 * The presentational implementation for the final result
 * after the client answers all the question.
 */
const Result = () => (
    <Container>
        <Text>Alegerea ta este:</Text>

        <TypeScriptLogo
            src={"https://www.sitepen.com/blog/wp-content/uploads/2016/04/typescript_heading.png"}
        />
    </Container>
);

export default Result;