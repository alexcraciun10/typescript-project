import styled from 'styled-components';

export const Container = styled.div`
    margin-top: 10%;
    width: 400px;
    position: absolute;
    display: block;
    margin-left: auto;
    margin-right: auto;

    opacity: 1;
	animation-name: fadeInOpacity;
	animation-iteration-count: 1;
	animation-timing-function: ease-in;
    animation-duration: 2s;
    
    @keyframes fadeInOpacity {
        0% { opacity: 0; }
        100% { opacity: 1; }
    }
`;

export const Text = styled.h1`
    font-family: Roboto;
    text-align: center;
`;

export const TypeScriptLogo = styled.img`
    height: 200px;
    width: 400px;
    border-radius: 5%;
    background-size: auto 100%;
    cursor: pointer;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 30px;
`;