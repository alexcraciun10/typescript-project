import * as React from 'react';
import { map } from 'lodash';

interface Props<T> {
    collection: T[];
    mapFunction: (element: T, ...parameters: any[]) => T;
}

/**
 * Render a collection of components after applying
 * the map function on each element.
 * @param props receives the collection and the map function
 */
const Map = (props: Props<any>) => (
    <>
        {map(props.collection, (element, index) => (
            <div key={index}>
                {props.mapFunction(element)}
            </div>
        ))}
    </>
);

export default Map;