import * as React from 'react';

interface Props {
    condition: boolean;
    children: React.ReactChild;
}

/**
 * Render the children only if the condition is true.
 * @param props 
 */
const If = (props: Props) => (
    <>
        {props.condition && props.children}
    </>
);

export default If;