import React from 'react';
import ReactDOM from 'react-dom';
import SmartApp from './components/specific/App';

ReactDOM.render(<SmartApp />, document.getElementById('root'));
